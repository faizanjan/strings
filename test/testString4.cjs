#!/usr/bin/env node

const getFullNameInTitleCase = require('../string4.cjs');
const nameObj = {"last_name": "SMith", "first_name": "JoHN", "middle_name": "doe"};

const result = getFullNameInTitleCase(nameObj);

console.log(result);