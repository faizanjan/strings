
const splitIP = ip =>{
    let arr = ip.split('.');
    for(let el of arr){
        let octet = parseInt(el);
        if(octet!=el || octet>255 || octet<0) return [];
    }
    return arr;
}
module.exports = splitIP