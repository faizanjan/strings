
const strToNum = str =>{
    let ans = '', index=1;

    //CHECK 1 : check if the number is negative, then start from index 2
    if (str.charAt(0) === '-' && str.charAt(1) === '$') {
        ans+='-';
        index ++;
    }
    // CHECK 2 : if the string neither starts with '-$' nor with '$', then it is invalid
    else if (str.charAt(0) !== '$') return 0;

    const subStr = str.substring(index); // will work on this string now to retrieve the number
    let decimalFlag= false;

    for(let i=0 ; i<subStr.length; i++){

        // CHECK 3 : checks if the decimals points are repeated more than once
        if(subStr.charAt(i)==='.'){
            if(decimalFlag) return 0;
            else {
                ans+='.';
                decimalFlag=true;
            }
        }

        // CHECK 4 : checks if the commas are placed correctly
        else if(subStr.charAt(i)===',') {
            if(subStr.charAt(i+1)===',' || 
            subStr.charAt(i+2)===',' || 
            i>=subStr.length-3 || 
            i===0 || (i===1 && subStr.charAt(0)==='-')) return 0;
            else continue;
        }

        // CHECK 5 : checks if the character is a digit or not
        else if(subStr.charCodeAt(i)> 47 && subStr.charCodeAt(i)< 58) ans += subStr.charAt(i);
        else return 0;        
    }
    
    return parseFloat(ans); 
}

module.exports = strToNum;
