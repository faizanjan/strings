
const fetchMonth = dateStr =>{
    let dateArr = dateStr.split('/');
    if(!checkDateValidity(dateArr)) return;
    else {
        return months[dateArr[1]]
    }
}

const months = {
    1 : "January",
    2 : "February",
    3 : "March",
    4 : "April",
    5 : "May",
    6 : "June",
    7 : "July",
    8 : "August",
    9 : "September",
    10 : "October",
    11 : "November",
    12 : "December"
}

const checkDateValidity = date =>{
    if (date[0]<1 || date[0]>31 || date[1]<0 || date[1]>12) return false;
    else if((date[1] == 4 ||
       date[1] == 6 ||
       date[1] == 9 ||
       date[1] == 11) && 
       date[0]>30) return false;
    else if(date[1]===2 && date[0]>28) return false;
    else return true;
}

module.exports = fetchMonth;