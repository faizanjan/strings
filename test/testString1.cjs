#!/usr/bin/env node

const strToNum = require('../string1.cjs');
const testStrings = ["$100.45", "$1,002.22", "-$123"]

for (let str of testStrings){
    const result = strToNum(str);
    console.log(result);
}