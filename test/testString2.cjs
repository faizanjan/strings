#!/usr/bin/env node

const splitIP = require('../string2.cjs');
const ipAdd = "111.139.100.143" ;

const result = splitIP(ipAdd);

if(result.length!==0) console.log(result);
else console.log("Pass valid IP address, only IPV4 addresses are supported");