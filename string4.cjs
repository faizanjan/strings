
const getFullName = nameObject =>{
    if(typeof(nameObject)!=='object' || nameObject.length===0){
        return ''; //check for argument validity
    }

    let {first_name=`\b`, middle_name=`\b`, last_name='\b'} = nameObject
    return toTitleCase(first_name)+' '+toTitleCase(middle_name)+' '+toTitleCase(last_name);
}

const toTitleCase = name=>{
    if(name.length===0) return '';
    name = name.toLowerCase();
    name = name.split('');
    name[0]= name[0].toUpperCase();
    name = name.join('');
    return name;
}

module.exports = getFullName;